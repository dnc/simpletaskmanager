﻿using System.Collections.Generic;
using SimpleTaskManager.Model.Models;

namespace SimpleTaskManager.Model.Services.Abstract
{
    /// <summary>
    /// A service that manages <see cref="Project"/> instances
    /// </summary>
    public interface IProjectService
    {
        /// <summary>
        /// Saves the given <see cref="Project"/>
        /// </summary>
        /// <param name="project">the <see cref="Project"/> to save</param>
        void Save(Project project);

        /// <summary>
        /// Finds all of the previously saved (<see cref="Save(Project)"/>) <see cref="Project"/> instances
        /// </summary>
        /// <returns>a <see cref="IEnumerable{Project}"/> containing all of the previously saved <see cref="Project"/> 
        /// instances</returns>
        IEnumerable<Project> FindAll();

        /// <summary>
        /// Finds all of the previously saved (<see cref="Save(Project)"/>) <see cref="Project"/> instances, to which
        /// the <see cref="User"/> with the given <see cref="User.Email"/> belongs to
        /// </summary>
        /// <returns>a <see cref="IEnumerable{Project}"/> containing all of the previously saved <see cref="Project"/> 
        /// instances, to which the <see cref="User"/> with the given <see cref="User.Email"/> belongs to</returns>
        IEnumerable<Project> FindAllByUserEmail(string userEmail);

        /// <summary>
        /// Removes the <see cref="Project"/> with the given <see cref="Project.Id"/>. Any related <see cref="Task"/>
        /// <see cref="UserProject"/> instances are also removed
        /// </summary>
        /// <param name="projectId">the <see cref="Project.Id"/> of the <see cref="Project"/> to remove</param>
        void RemoveById(long projectId);
    }
}
