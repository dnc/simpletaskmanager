﻿using SimpleTaskManager.Model.Models;

namespace SimpleTaskManager.Model.Services.Abstract
{
    public interface IUserProjectService
    {
        /// <summary>
        /// Associates the given <see cref="User"/> and <see cref="Project"/> instances 
        /// </summary>
        /// <param name="user">the <see cref="User"/> to associate with the given <see cref="Project"/></param>
        /// <param name="project">the <see cref="Project"/> to associate with the given <see cref="User"/></param>
        void Associate(User user, Project project);

        /// <summary>
        /// Cancels any association between the given <see cref="User"/> and <see cref="Project"/> instances 
        /// </summary>
        /// <param name="user">the <see cref="User"/> to disassociate from the given <see cref="Project"/></param>
        /// <param name="project">the <see cref="Project"/> to disassociate from the given <see cref="User"/></param>
        void Disassociate(User user, Project project);
    }
}
