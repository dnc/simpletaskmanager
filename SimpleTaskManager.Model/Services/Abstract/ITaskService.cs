﻿using System.Collections.Generic;
using SimpleTaskManager.Model.Models;

namespace SimpleTaskManager.Model.Services.Abstract
{
    /// <summary>
    /// A service that manages <see cref="Task"/> instances
    /// </summary>
    public interface ITaskService
    {
        /// <summary>
        /// Saves the given <see cref="Task"/>
        /// </summary>
        /// <param name="task">the <see cref="Task"/> to save</param>
        void Save(Task task);

        /// <summary>
        /// Finds all of the previously saved (<see cref="Save(Task)"/>) <see cref="Task"/> instances
        /// </summary>
        /// <returns>a <see cref="IEnumerable{Task}"/> containing all of the previously saved <see cref="Task"/> 
        /// instances</returns>
        IEnumerable<Task> FindAll();

        /// <summary>
        /// Finds all of the previously saved (<see cref="Save(Task)"/>) <see cref="Task"/> instances that are
        /// associated with the <see cref="Project"/> with the given <see cref="Project.Id"/>
        /// </summary>
        /// <param name="projectId">the <see cref="Project.Id"/></param>
        /// <returns>a <see cref="IEnumerable{Task}"/> containing all of the previously saved <see cref="Task"/> 
        /// instances that are associated with the <see cref="Project"/> with the given <see cref="Project.Id"/>
        /// </returns>
        IEnumerable<Task> FindAllByProjectId(long projectId);

        /// <summary>
        /// Removes the <see cref="Task"/> with the given <see cref="Task.Id"/>
        /// </summary>
        /// <param name="taskId">the <see cref="Task.Id"/> of the <see cref="Task"/> to remove</param>
        void RemoveById(long taskId);
    }
}
