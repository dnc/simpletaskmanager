﻿using System.Collections.Generic;
using SimpleTaskManager.Model.Models;

namespace SimpleTaskManager.Model.Services.Abstract
{
    /// <summary>
    /// A service that manages <see cref="User"/> instances
    /// </summary>
    public interface IUserService
    {
        /// <summary>
        /// Saves the given <see cref="User"/>
        /// </summary>
        /// <param name="user">the <see cref="User"/> to save</param>
        void Save(User user);

        /// <summary>
        /// Attempts to find a previously saved (<see cref="Save(User)"/>) <see cref="User"/>, based on its 
        /// <see cref="User.Email"/> property
        /// </summary>
        /// <param name="email">the email address to search by</param>
        /// <returns>a see <see cref="User"/>, matching the given <paramref name="email"/>, or <see langword="null"/>
        /// if there is no match</returns>
        User FindByEmail(string email);

        /// <summary>
        /// Finds all of the previously saved (<see cref="Save(User)"/>) <see cref="User"/> instances
        /// </summary>
        /// <returns>a <see cref="IEnumerable{User}"/> containing all of the previously saved <see cref="User"/> 
        /// instances</returns>
        IEnumerable<User> FindAll();

        /// <summary>
        /// Checks if the given credential pair matches the credentials (<see cref="User.Email"/> and
        /// <see cref="User.Password"/>) of any of the previously saved <see cref="User"/> instances
        /// </summary>
        /// <param name="email">the value corresponding to a <see cref="User"/> instance's <see cref="User.Email"/></param>
        /// <param name="password">the value corresponding to a <see cref="User"/> instance's <see cref="User.Password"/></param>
        /// <returns><see langword="true"/>, if the given credentials match the credentials of a previously saved
        /// <see cref="User"/> instance</returns>
        bool CanAuthenticate(string email, string password);
    }
}
