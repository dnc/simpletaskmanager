﻿namespace SimpleTaskManager.Model.Models
{
    /// <summary>
    /// Represents the many-to-many relationship between <see cref="User"/> and <see cref="Project"/> instances
    /// </summary>
    public class UserProject
    {
        public long UserId { get; set; }

        public virtual User User { get; set; }

        public long ProjectId { get; set; }

        public virtual Project Project { get; set; }

        public UserProject(User user, Project project)
        {
            User = user;
            UserId = user.Id;

            Project = project;
            ProjectId = project.Id;
        }

        /// <summary>
        /// Required by EntityFramework
        /// </summary>
        protected UserProject()
        {

        }
    }
}
