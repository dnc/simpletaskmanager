﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleTaskManager.Model.Models
{
    /// <summary>
    /// Represents a project. Each project contains zero or more tasks
    /// </summary>
    public class Project
    {
        /// <summary>
        /// A value that uniquely identifies this <see cref="Project"/>
        /// </summary>
        public long Id { get; set; }

        public string Name { get; set; }


        #region Navigation properties

        /// <summary>
        /// The list of <see cref="Task"/> instances that belong to this <see cref="Project"/>
        /// </summary>
        public virtual ICollection<Task> Tasks { get; set; }

        /// <summary>
        /// The list of <see cref="User"/> instances associated with this <see cref="Project"/>
        /// </summary>
        public ICollection<User> Team => UserProjects?.Select(userProject => userProject.User).ToList();

        /// <summary>
        /// Necessary in order to be able to model the many-to-many relationship between <see cref="User"/>
        /// instances and <see cref="Project"/> instances
        /// </summary>
        public virtual ICollection<UserProject> UserProjects { get; set; }

        #endregion


        public Project(string name)
        {
            Name = name;
        }
    }
}
