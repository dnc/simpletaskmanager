﻿namespace SimpleTaskManager.Model.Models
{
    /// <summary>
    /// Represents a project task
    /// </summary>
    public class Task
    {
        /// <summary>
        /// A value that uniquely identifies this <see cref="Task"/>
        /// </summary>
        public long Id { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public StatusType Status { get; set; }

        /// <summary>
        /// The time (in minutes) allotted for this <see cref="Task"/>
        /// </summary>
        public double AllocatedTime { get; set; }

        /// <summary>
        /// The <see cref="User.Id"/> of the <see cref="Models.User"/> instance this <see cref="Task"/> belongs to
        /// </summary>
        public long UserId { get; set; }

        /// <summary>
        /// The <see cref="Project.Id"/> of the <see cref="Models.Project"/> instance this <see cref="Task"/> belongs to
        /// </summary>
        public long ProjectId { get; set; }


        #region Navigation properties

        /// <summary>
        /// The <see cref="Models.User"/> to which this <see cref="Task"/> belongs
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// The <see cref="Models.Project"/> to which this <see cref="Task"/> belongs
        /// </summary>
        public virtual Project Project { get; set; }

        #endregion


        public Task(string name, string description, double allocatedTime, User user, Project project, StatusType status)
        {
            Name = name;
            Description = description;
            AllocatedTime = allocatedTime;
            User = user;
            Project = project;
            Status = status;
        }

        /// <summary>
        /// Required by EntityFramework
        /// </summary>
        protected Task()
        {

        }

        public enum StatusType
        {
            PENDING,
            COMPLETED
        }
    }
}
