﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleTaskManager.Model.Models
{
    /// <summary>
    /// Represents an application user
    /// </summary>
    public class User
    {
        /// <summary>
        /// A value that uniquely identifies this <see cref="User"/>
        /// </summary>
        public long Id { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        /// <summary>
        /// An email address. It uniquely identifies this <see cref="User"/>
        /// </summary>
        public string Email { get; set; }

        public string Password { get; set; }


        #region Navigation properties

        /// <summary>
        /// The list of <see cref="Task"/> instances that belong to this <see cref="Project"/>
        /// </summary>
        public virtual ICollection<Task> Tasks { get; set; }


        /// <summary>
        /// The list of <see cref="Project"/> instances this <see cref="User"/> is associated with
        /// </summary>
        public ICollection<Project> Projects => UserProjects.Select(userProject => userProject.Project).ToList();

        /// <summary>
        /// Necessary in order to be able to model the many-to-many relationship between <see cref="User"/>
        /// instances and <see cref="Project"/> instances
        /// </summary>
        public virtual ICollection<UserProject> UserProjects { get; set; }

        #endregion


        public User(string firstName, string lastName, string email, string password)
        {
            FirstName = firstName;
            LastName = lastName;
            Email = email;
            Password = password;
        }
    }
}
