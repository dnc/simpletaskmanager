﻿using System.ComponentModel.DataAnnotations;

namespace SimpleTaskManager.App.Wpf.Extensions
{
    /// <summary>
    /// Contains a collection of <see langword="string"/> related extension methods
    /// </summary>
    internal static class StringExtensions
    {
        private static EmailAddressAttribute emailAddressAttribute;

        static StringExtensions()
        {
            emailAddressAttribute = new EmailAddressAttribute();
        }

        /// <summary>
        /// Checks if the given string is a valid email address
        /// </summary>
        /// <param name="input">the string to check</param>
        /// <returns><see langword="true"/>, if the given string is a valid email address</returns>
        public static bool IsEmailAddress(this string input)
        {
            return emailAddressAttribute.IsValid(input);
        }
    }
}
