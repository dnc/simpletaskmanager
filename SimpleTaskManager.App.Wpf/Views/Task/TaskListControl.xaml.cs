﻿using System;
using System.Windows;
using System.Windows.Controls;
using SimpleTaskManager.App.Wpf.ViewModels;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for TaskListControl.xaml
    /// </summary>
    public partial class TaskListControl : UserControl
    {
        private readonly ITaskService taskService;

        private readonly Project selectedProject;

        private readonly User authenticatedUser;

        private readonly Action backButtonClickCallback;

        private TaskListViewModel taskListViewModel;

        private readonly bool authenticatedUserIsAssociatedWithSelectedProject;

        private Window taskInfoDialog;

        public TaskListControl(ITaskService taskService, Project selectedProject, User authenticatedUser, Action taskListBackButtonPressCallback)
        {
            InitializeComponent();

            this.taskService = taskService;
            this.selectedProject = selectedProject;
            this.authenticatedUser = authenticatedUser;
            backButtonClickCallback = taskListBackButtonPressCallback;

            taskListViewModel = new TaskListViewModel(taskService, selectedProject);
            DataContext = taskListViewModel;

            authenticatedUserIsAssociatedWithSelectedProject = selectedProject.Team != null
                && selectedProject.Team.Contains(authenticatedUser);

            DisableTaskManagementButtonsIfRequired();
        }

        /// <summary>
        /// Disables the <see cref="NewTaskButton"/>, <see cref="RemoveTaskButton"/> buttons, if the currently 
        /// authenticated user is not associated with the currently selected project
        /// </summary>
        private void DisableTaskManagementButtonsIfRequired()
        {
            NewTaskButton.IsEnabled = RemoveTaskButton.IsEnabled = authenticatedUserIsAssociatedWithSelectedProject;
        }

        /// <summary>
        /// Method called when the "New" button is pressed. Creates and opens a new <see cref="TaskInfoDialog"/> instance
        /// </summary>
        private void AddNewTaskButtonClick(object sender, RoutedEventArgs e)
        {
            taskInfoDialog = new TaskInfoDialog(taskService, selectedProject, SuccessfulTaskPersistenceCallback)
            {
                Title = "New task"
            };

            taskInfoDialog.ShowDialog();
        }

        /// <summary>
        /// Method called when the "Back" button is pressed. Calls the <see cref="backButtonClickCallback"/>
        /// delegate
        /// </summary>
        private void BackButtonClick(object sender, RoutedEventArgs e)
        {
            backButtonClickCallback();
        }

        private void SuccessfulTaskPersistenceCallback()
        {
            taskInfoDialog.Close();
            taskListViewModel.ReloadTasks();
        }

        /// <summary>
        /// Method called when a <see cref="DataGrid"/> row is double clicked
        /// </summary>
        private void RowDoubleClickHandler(object sender, System.Windows.Input.MouseButtonEventArgs e)
        {
            if (!authenticatedUserIsAssociatedWithSelectedProject)
            {
                return;
            }

            var selectedTask = (Task) TaskListDataGrid.SelectedItem;

            taskInfoDialog = new TaskInfoDialog(taskService, selectedProject, SuccessfulTaskPersistenceCallback, selectedTask)
            {
                Title = "Existing task"
            };

            taskInfoDialog.ShowDialog();
        }
    }
}
