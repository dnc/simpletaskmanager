﻿using System;
using System.Windows;
using SimpleTaskManager.App.Wpf.ViewModels;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for TaskInfoDialog.xaml
    /// </summary>
    public partial class TaskInfoDialog : Window
    {
        public TaskInfoDialog(ITaskService taskService, Project selectedProject, Action successfulTaskPersistenceCallback, Task selectedTask = null)
        {
            InitializeComponent();

            DataContext = new TaskInfoViewModel(taskService, selectedProject, successfulTaskPersistenceCallback, selectedTask);
        }
    }
}
