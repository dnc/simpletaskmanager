﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using SimpleTaskManager.App.Wpf.Data;
using SimpleTaskManager.App.Wpf.Data.Services;
using SimpleTaskManager.App.Wpf.ViewModels;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for ProjectListControl.xaml
    /// </summary>
    public partial class ProjectListControl : UserControl
    {
        private readonly AppDbContext appDbContext;

        private readonly IProjectService projectService;

        private readonly User authenticatedUser;

        private Window newProjectDialog;

        private Panel projectListParentPanel;

        private readonly ProjectListViewModel projectListViewModel;

        public ProjectListControl(AppDbContext appDbContext, IProjectService projectService,
            IUserProjectService userProjectService, User authenticatedUser)
        {
            InitializeComponent();

            projectListViewModel = new ProjectListViewModel(projectService, userProjectService, authenticatedUser);
            DataContext = projectListViewModel;

            this.appDbContext = appDbContext;
            this.projectService = projectService;
            this.authenticatedUser = authenticatedUser;
        }

        /// <summary>
        /// Method called when the "New" button is pressed. Creates and opens a new <see cref="NewProjectDialog"/> instance
        /// </summary>
        private void AddNewProjectButtonClick(object sender, RoutedEventArgs e)
        {
            newProjectDialog = new NewProjectDialog(projectService, SuccessfulProjectRegistrationCallback);
            newProjectDialog.ShowDialog();
        }

        /// <summary>
        /// Method to be called when a <see cref="Project"/> is successfully saved
        /// </summary>
        private void SuccessfulProjectRegistrationCallback()
        {
            newProjectDialog.Close();
            projectListViewModel.ReloadProjects();
        }

        /// <summary>
        /// Method called when a <see cref="DataGrid"/> row is double clicked
        /// </summary>
        private void RowDoubleClickHandler(object sender, MouseButtonEventArgs e)
        {
            var selectedProjectInfo = (ProjectListViewModel.ProjectInfo) ProjectListDataGrid.SelectedItem;

            var taskService = new TaskService(appDbContext);
            var taskListControl = new TaskListControl(taskService, selectedProjectInfo.Project, authenticatedUser, TaskListBackButtonClickCallback);

            // Backs up the project list DataGrid and all of its siblings
            projectListParentPanel = ProjectListParentPanel;

            RootPanel.Children.Clear();
            RootPanel.Children.Add(taskListControl);
        }

        /// <summary>
        /// Method called the "Back" button in <see cref="TaskListControl"/> is clicked 
        /// </summary>
        private void TaskListBackButtonClickCallback()
        {
            projectListViewModel.ReloadProjects();

            RootPanel.Children.Clear();
            RootPanel.Children.Add(projectListParentPanel);
        }
    }
}
