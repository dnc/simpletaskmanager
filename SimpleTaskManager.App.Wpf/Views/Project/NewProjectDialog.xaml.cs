﻿using System;
using System.Windows;
using SimpleTaskManager.App.Wpf.ViewModels;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for NewProjectDialog.xaml
    /// </summary>
    public partial class NewProjectDialog : Window
    {
        public NewProjectDialog(IProjectService projectService, Action successfulProjectRegistrationCallback)
        {
            InitializeComponent();

            DataContext = new ProjectRegistrationViewModel(projectService, successfulProjectRegistrationCallback);
        }
    }
}
