﻿using System;
using System.IO;
using System.Windows;
using System.Windows.Threading;
using Microsoft.EntityFrameworkCore;
using SimpleTaskManager.App.Wpf.Data;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        /// <summary>
        /// The name of the error file. The error file will contain the last unhandled exception thrown
        /// </summary>
        private const string ERROR_FILE_NAME = "error.log";

        private AppDbContext context;

        private Window mainWindow;

        public App()
        {
            context = new AppDbContext();

            DeleteErrorFile();
            EnsureDatabaseExists();
        }

        /// <summary>
        /// Deletes any file named after <see cref="ERROR_FILE_NAME"/>, that resides in the current working directory
        /// </summary>
        private void DeleteErrorFile()
        {
            File.Delete(ERROR_FILE_NAME);
        }

        /// <summary>
        /// Creates a file named after <see cref="ERROR_FILE_NAME"/>, in the current working directory. The file will
        /// contain the provided <paramref name="error"/>
        /// </summary>
        private void CreateErrorFile(string error)
        {
            File.WriteAllText(ERROR_FILE_NAME, error);
        }

        /// <summary>
        /// Creates the application database if it doesn't exist
        /// </summary>
        private void EnsureDatabaseExists()
        {
            context.Database.Migrate();
        }

        /// <summary>
        /// Called when the application is started. It displays the main (and only) window of the application
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ApplicationStartup(object sender, StartupEventArgs e)
        {
            mainWindow = new MainWindow(context);

            mainWindow.Closed += MainWindowClosed;

            mainWindow.Show();
        }

        /// <summary>
        /// Called then the main (and only) window of the application is closed. This method releases the 
        /// <see cref="AppDbContext"/> instance
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MainWindowClosed(object sender, System.EventArgs e)
        {
            context.Dispose();
        }

        /// <summary>
        /// Called when an unhandled exception occurs
        /// </summary>
        private void UnhandledExceptionHandler(object sender, DispatcherUnhandledExceptionEventArgs e)
        {
            CreateErrorFile(e.Exception.ToString());

            string fullErrorFilePath = Path.Combine(Environment.CurrentDirectory, ERROR_FILE_NAME);
            string message = $@"Please contact the developer by supplying the content of the file located at {fullErrorFilePath}";

            MessageBox.Show(message, "An error occurred", MessageBoxButton.OK, MessageBoxImage.Error);

            Shutdown(-1);
        }
    }
}
