﻿using System;
using System.Windows.Controls;
using SimpleTaskManager.App.Wpf.ViewModels;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for UserRegistrationControl.xaml
    /// </summary>
    public partial class UserRegistrationControl : UserControl
    {
        public UserRegistrationControl(IUserService userService, Action<string, string> successfulRegistrationCallback)
        {
            InitializeComponent();

            DataContext = new UserRegistrationViewModel(userService, successfulRegistrationCallback);
        }
    }
}
