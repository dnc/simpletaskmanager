﻿using System;
using System.Windows.Controls;
using SimpleTaskManager.App.Wpf.ViewModels;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for UserAuthControl.xaml
    /// </summary>
    public partial class UserAuthControl : UserControl
    {
        public UserAuthViewModel UserAuthViewModel { get; set; }

        public UserAuthControl(IUserService userService, string email, string password,
            Action<User> successfulLoginCallback)
        {
            InitializeComponent();

            UserAuthViewModel = new UserAuthViewModel(userService, successfulLoginCallback)
            {
                Email = email,
                Password = password
            };

            DataContext = UserAuthViewModel;
        }
    }
}
