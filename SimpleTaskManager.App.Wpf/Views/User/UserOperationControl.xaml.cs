﻿using System;
using System.Windows;
using System.Windows.Controls;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for UserOperationControl.xaml
    /// </summary>
    public partial class UserOperationControl : UserControl
    {
        private readonly IUserService userService;
        private readonly Action<User> successfulLoginCallback;

        public UserOperationControl(IUserService userService, Action<User> successfulLoginCallback)
        {
            InitializeComponent();

            this.userService = userService;
            this.successfulLoginCallback = successfulLoginCallback;

            ShowUserAuthControl();
        }

        private void ShowUserAuthControl(string email = null, string password = null)
        {
            UserRegistrationHyperlink.Visibility = Visibility.Visible;

            // Removes the UserRegistrationControl, which exists if called via SuccessfulRegistrationCallback()
            ContentPanel.Children.Clear();

            ContentPanel.Children.Add(new UserAuthControl(userService, email, password, successfulLoginCallback));
        }

        private void ShowUserRegistrationControl()
        {
            UserRegistrationHyperlink.Visibility = Visibility.Hidden;

            // Removes the UserAuthControl, which is added first
            ContentPanel.Children.Clear();

            ContentPanel.Children.Add(new UserRegistrationControl(userService, SuccessfulRegistrationCallback));
        }

        private void UserRegistrationHyperlinkClick(object sender, RoutedEventArgs e)
        {
            ShowUserRegistrationControl();
        }

        /// <summary>
        /// Method to be called when a successful registration takes place
        /// </summary>
        /// <param name="email">the <see cref="User.Email"/> used in registration</param>
        /// <param name="password">the <see cref="User.Password"/> used in registration</param>
        private void SuccessfulRegistrationCallback(string email, string password)
        {
            ShowUserAuthControl(email, password);
        }
    }
}
