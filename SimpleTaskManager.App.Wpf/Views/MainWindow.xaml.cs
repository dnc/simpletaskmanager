﻿using System.Windows;
using SimpleTaskManager.App.Wpf.Data;
using SimpleTaskManager.App.Wpf.Data.Services;
using SimpleTaskManager.Model.Models;

namespace SimpleTaskManager.App.Wpf.Views
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private User authenticatedUser;

        private readonly AppDbContext appDbContext;

        public MainWindow(AppDbContext appDbContext)
        {
            InitializeComponent();

            this.appDbContext = appDbContext;

            ShowUserOperationControl();
        }

        /// <summary>
        /// Sets a <see cref="UserOperationControl"/> instance as the root view of the window
        /// </summary>
        private void ShowUserOperationControl()
        {
            var userService = new UserService(appDbContext);
            var userOperationControl = new UserOperationControl(userService, SuccessfulLoginCallback);

            RootPanel.Children.Add(userOperationControl);
        }

        /// <summary>
        /// Method to be called when a successful login takes place
        /// </summary>
        /// <param name="authenticatedUser">the <see cref="User"/> instance corresponding to the authenticated user</param>
        private void SuccessfulLoginCallback(User authenticatedUser)
        {
            this.authenticatedUser = authenticatedUser;
            ShowProjectListControl();
        }

        /// <summary>
        /// Sets a <see cref="ProjectListControl"/> instance as the root view of the window
        /// </summary>
        private void ShowProjectListControl()
        {
            var projectService = new ProjectService(appDbContext);
            var userProjectService = new UserProjectService(appDbContext);
            var projectListControl = new ProjectListControl(appDbContext, projectService, userProjectService, authenticatedUser);

            RootPanel.Children.Clear();
            RootPanel.Children.Add(projectListControl);
        }
    }
}
