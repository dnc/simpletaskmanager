﻿using System.Collections.Generic;
using System.Linq;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Data.Services
{
    /// <summary>
    /// A <see cref="ITaskService"/> implementation that uses EntityFramework
    /// </summary>
    internal class TaskService : ITaskService
    {
        private readonly AppDbContext context;

        public TaskService(AppDbContext context)
        {
            this.context = context;
        }

        public void Save(Task task)
        {
            if (context.Tasks.Any(t => t.Id == task.Id))
            {
                // Already exists, simply apply any changes made to the object
                context.SaveChanges();
                return;
            }

            context.Tasks.Add(task);
            context.SaveChanges();
        }

        public IEnumerable<Task> FindAll()
        {
            return context.Tasks.ToList();
        }

        public IEnumerable<Task> FindAllByProjectId(long projectId)
        {
            return context.Tasks.Where(task => task.ProjectId == projectId);
        }

        public void RemoveById(long taskId)
        {
            context.Tasks.RemoveRange(context.Tasks.Where(task => task.Id == taskId));
            context.SaveChanges();
        }
    }
}
