﻿using System.Collections.Generic;
using System.Linq;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Data.Services
{
    /// <summary>
    /// A <see cref="IProjectService"/> implementation that uses EntityFramework
    /// </summary>
    internal class ProjectService : IProjectService
    {
        private readonly AppDbContext context;

        public ProjectService(AppDbContext context)
        {
            this.context = context;
        }

        public void Save(Project project)
        {
            if (context.Projects.Any(p => p.Id == project.Id))
            {
                // Already exists, simply apply any changes made to the object
                context.SaveChanges();
                return;
            }

            context.Projects.Add(project);
            context.SaveChanges();
        }

        public IEnumerable<Project> FindAllByUserEmail(string userEmail)
        {
            return context.Projects.Where(project => project.Team.Any(user => user.Email == userEmail)).ToList();
        }

        public IEnumerable<Project> FindAll()
        {
            return context.Projects.ToList();
        }

        public void RemoveById(long projectId)
        {
            context.Projects.RemoveRange(context.Projects.Where(project => project.Id == projectId));
            context.SaveChanges();
        }
    }
}
