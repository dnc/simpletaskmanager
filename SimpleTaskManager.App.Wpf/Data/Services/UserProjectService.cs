﻿using System.Linq;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Data.Services
{
    /// <summary>
    /// A <see cref="IUserProjectService"/> implementation that uses EntityFramework
    /// </summary>
    internal class UserProjectService : IUserProjectService
    {
        private readonly AppDbContext context;

        public UserProjectService(AppDbContext context)
        {
            this.context = context;
        }

        public void Associate(User user, Project project)
        {
            if (context.UserProjects.Any(userProject =>
                userProject.UserId == user.Id && userProject.ProjectId == project.Id))
            {
                // An association already exists
                return;
            }

            context.UserProjects.Add(new UserProject(user, project));
            context.SaveChanges();
        }

        public void Disassociate(User user, Project project)
        {
            context.UserProjects.RemoveRange(
                context.UserProjects.Where(it => it.UserId == user.Id && it.ProjectId == project.Id));

            context.SaveChanges();
        }
    }
}
