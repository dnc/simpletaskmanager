﻿using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.Data.Services
{
    /// <summary>
    /// A <see cref="IUserService"/> implementation that uses EntityFramework
    /// </summary>
    internal class UserService : IUserService
    {
        private readonly AppDbContext context;

        public UserService(AppDbContext context)
        {
            this.context = context;
        }

        public void Save(User user)
        {
            // If this user was never persisted, replace the plain text password for its hash, in order to improve security
            if (!context.Users.Any(u => u.Email == user.Email))
            {
                user.Password = HashPassword(user.Password);
            }

            context.Users.Add(user);
            context.SaveChanges();
        }

        public User FindByEmail(string email)
        {
            return context.Users.Where(user => user.Email == email).SingleOrDefault();
        }

        public IEnumerable<User> FindAll()
        {
            return context.Users.ToList();
        }

        public bool CanAuthenticate(string email, string password)
        {
            User user = FindByEmail(email);

            if (user == null)
            {
                return false;
            }

            return user.Password == HashPassword(password);
        }

        /// <summary>
        /// Hashes the given password using the <see cref="MD5"/> hashing algorithm
        /// </summary>
        /// <param name="password">the password to hash</param>
        /// <returns>a string, representing the hash of the given <paramref name="password"/></returns>
        private string HashPassword(string password)
        {
            // See: https://stackoverflow.com/questions/11454004/calculate-a-md5-hash-from-a-string

            using (var sha = MD5.Create())
            {
                byte[] hash = sha.ComputeHash(Encoding.UTF8.GetBytes(password));

                // convert each of the bytes to their hexadecimal representation
                return string.Join("", hash.Select(it => it.ToString("X2")));
            }
        }
    }
}
