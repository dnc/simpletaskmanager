﻿using System.Configuration;
using Microsoft.EntityFrameworkCore;
using SimpleTaskManager.Model.Models;

namespace SimpleTaskManager.App.Wpf.Data
{
    /// <summary>
    /// An EntityFramework <see cref="DbContext"/> implementation
    /// </summary>
    public class AppDbContext : DbContext
    {
        /// <summary>
        /// The maximum length of text containing columns
        /// </summary>
        private const int MAX_COLUMN_LENGTH = 20;

        /// <summary>
        /// The maximum length of description containing columns
        /// </summary>
        private const int MAX_DESCRIPTION_COLUMN_LENGTH = 200;

        /// <summary>
        /// The maximum length of password containing columns. This has to be higher, especially if the password's hash
        /// is the one that's stored in the database. SHA512 = 128 chars, SHA256 = 64 chars, MD5 = 32 chars
        /// </summary>
        private const int MAX_PASSWORD_COLUMN_LENGTH = 128;

        /// <summary>
        /// An connection string labeled as "default" in App.config
        /// </summary>
        private string DefaultConnectionString { get; } = ConfigurationManager.ConnectionStrings["default"].ConnectionString;


        #region DbSet instances

        public DbSet<User> Users { get; set; }

        public DbSet<Task> Tasks { get; set; }

        public DbSet<Project> Projects { get; set; }

        public DbSet<UserProject> UserProjects { get; set; }

        #endregion


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            // Calling UseLazyLoadingProxies() enables on-access navigational property loading

            optionsBuilder.UseLazyLoadingProxies().UseSqlServer(DefaultConnectionString);
        }

        /// <summary>
        /// Configures model (<see cref="User"/>, <see cref="Task"/>, <see cref="Project"/>, etc.) classes as database 
        /// tables
        /// </summary>
        /// <param name="modelBuilder"></param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            ConfigureUserEntity(modelBuilder);
            ConfigureTaskEntity(modelBuilder);
            ConfigureProjectEntity(modelBuilder);
            ConfigureUserProjectEntity(modelBuilder);
        }

        private void ConfigureUserEntity(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<User>(userModelBuilder =>
            {
                userModelBuilder.Property(user => user.FirstName).IsRequired().HasMaxLength(MAX_COLUMN_LENGTH);
                userModelBuilder.Property(user => user.LastName).IsRequired().HasMaxLength(MAX_COLUMN_LENGTH);

                userModelBuilder.Property(user => user.Email).IsRequired().HasMaxLength(MAX_COLUMN_LENGTH);
                userModelBuilder.HasIndex(user => user.Email).IsUnique();

                userModelBuilder.Property(user => user.Password).IsRequired().HasMaxLength(MAX_PASSWORD_COLUMN_LENGTH);
            });
        }

        private void ConfigureTaskEntity(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Task>(taskModelBuilder =>
            {
                taskModelBuilder.Property(task => task.Name).IsRequired().HasMaxLength(MAX_COLUMN_LENGTH);
                taskModelBuilder.Property(task => task.Description).IsRequired().HasMaxLength(MAX_DESCRIPTION_COLUMN_LENGTH);

                taskModelBuilder.Property(task => task.Status).IsRequired();
            });
        }

        private void ConfigureProjectEntity(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Project>(projectModelBuilder =>
            {
                projectModelBuilder.Property(project => project.Name).IsRequired().HasMaxLength(MAX_COLUMN_LENGTH);
            });
        }

        private void ConfigureUserProjectEntity(ModelBuilder modelBuilder)
        {
            /*
             * Many-to-many relationships are not supported (EF 2.2.2) out of the box. We have to configure two separate 
             * one-to-many relationships
             * 
             * See https://docs.microsoft.com/en-us/ef/core/modeling/relationships#many-to-many
             */

            modelBuilder.Entity<UserProject>(userProjectModelBuilder =>
            {
                userProjectModelBuilder.HasKey(userProject => new { userProject.UserId, userProject.ProjectId });

                userProjectModelBuilder.HasOne(userProject => userProject.User)
                    .WithMany(user => user.UserProjects).HasForeignKey(userProject => userProject.UserId);

                userProjectModelBuilder.HasOne(userProject => userProject.Project)
                    .WithMany(project => project.UserProjects).HasForeignKey(userProject => userProject.ProjectId);
            });
        }
    }
}
