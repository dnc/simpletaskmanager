﻿using System.Linq;

namespace SimpleTaskManager.App.Wpf.Utils
{
    /// <summary>
    /// Contains a collection of <see langword="string"/> related helper methods
    /// </summary>
    internal static class StringUtil
    {
        /// <summary>
        /// Checks if any of the given strings is null or whitespace
        /// </summary>
        /// <param name="strings">the list of strings to check</param>
        /// <returns><see langword="true"/>, if any of the given strings is null or whitespace, false if none of the strings are null or
        /// whitespace, or if the given list is empty</returns>
        public static bool AnyIsNullOrWhiteSpace(params string[] strings)
        {
            if (!strings.Any())
            {
                return false;
            }

            foreach (string str in strings)
            {
                if (string.IsNullOrWhiteSpace(str))
                {
                    return true;
                }
            }

            return false;
        }
    }
}
