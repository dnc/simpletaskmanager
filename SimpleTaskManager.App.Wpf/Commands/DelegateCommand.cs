﻿using System;
using System.Windows.Input;

namespace SimpleTaskManager.App.Wpf.Commands
{
    public class DelegateCommand : ICommand
    {
        private readonly Action execute;

        private readonly Func<bool> canExecute;

        public event EventHandler CanExecuteChanged;

        /// <summary>
        /// Constructs a new <see cref="DelegateCommand"/> using the given <paramref name="canExecute"/> parameter in the
        /// call to <see cref="CanExecute(object)"/> and the given <paramref name="execute"/> parameter in the call to
        /// <see cref="Execute(object)"/>
        /// </summary>
        /// <param name="execute">the <see cref="Action"/> to call when <see cref="Execute(object)"/> is called</param>
        /// <param name="canExecute">the <see cref="Func{TResult}> to call when <see cref="CanExecute(object)"/> is 
        /// called<"/></param>
        public DelegateCommand(Action execute, Func<bool> canExecute)
        {
            this.execute = execute;
            this.canExecute = canExecute;
        }

        /// <summary>
        /// Returns <see langword="true"/> if the <see cref="Execute(object)"/> method may be safely called
        /// </summary>
        /// <param name="parameter">unused</param>
        /// <returns><see langword="true"/> if the <see cref="Execute(object)"/> method may be safely called</returns>
        public bool CanExecute(object parameter)
        {
            return canExecute();
        }

        /// <summary>
        /// Calls the <see cref="Action"/> given in the constructor
        /// </summary>
        /// <param name="parameter">unused</param>
        public void Execute(object parameter)
        {
            execute();
        }

        /// <summary>
        /// Raises the <see cref="CanExecuteChanged"/> event if there's at least one subscriber
        /// </summary>
        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }
    }
}
