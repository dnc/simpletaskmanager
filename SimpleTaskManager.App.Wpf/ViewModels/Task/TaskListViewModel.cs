﻿using System.Collections.ObjectModel;
using SimpleTaskManager.App.Wpf.Commands;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.ViewModels
{
    internal class TaskListViewModel
    {
        private readonly ITaskService taskService;

        public Project SelectedProject { get; }

        public ObservableCollection<Task> Tasks { get; }

        public DelegateCommand SelectedTaskRemovalCommand { get; }

        private Task selectedTask;

        public Task SelectedTask
        {
            get => selectedTask;

            set
            {
                selectedTask = value;
                SelectedTaskRemovalCommand?.RaiseCanExecuteChanged();
            }
        }

        public TaskListViewModel(ITaskService taskService, Project selectedProject)
        {
            this.taskService = taskService;

            SelectedProject = selectedProject;

            Tasks = new ObservableCollection<Task>();

            SelectedTaskRemovalCommand = new DelegateCommand(RemoveSelectedTask, IsSelectedTaskNotNull);

            ReloadTasks();
        }

        /// <summary>
        /// Updates the <see cref="Tasks"/> list by re-reading the <see cref="Task"/> instance list via
        /// the provided service's <see cref="ITaskService.FindAll()"/> method
        /// </summary>
        public void ReloadTasks()
        {
            Tasks.Clear();

            foreach (Task task in taskService.FindAllByProjectId(SelectedProject.Id))
            {
                Tasks.Add(task);
            }
        }

        /// <summary>
        /// Checks if <see cref="SelectedTask"/> is not null
        /// </summary>
        /// <returns><see langword="true"/>, if <see cref="SelectedTask"/> is not null</returns>
        private bool IsSelectedTaskNotNull()
        {
            return SelectedTask != null;
        }

        /// <summary>
        /// Removes <see cref="SelectedTask"/> from the <see cref="Tasks"/> list and via the provided service's
        /// <see cref="ITaskService.RemoveById(long)"/> method
        /// </summary>
        private void RemoveSelectedTask()
        {
            taskService.RemoveById(SelectedTask.Id);
            Tasks.Remove(SelectedTask);
        }
    }
}
