﻿using System;
using System.Collections.Generic;
using System.Linq;
using SimpleTaskManager.App.Wpf.Commands;
using SimpleTaskManager.App.Wpf.Utils;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.ViewModels
{
    internal class TaskInfoViewModel
    {
        private readonly ITaskService taskService;

        private readonly Project selectedProject;

        private readonly Action successfulTaskPersistenceCallback;

        private readonly Task existingTask;

        #region Public properties

        private string name;

        public string Name
        {
            get => name;

            set
            {
                name = value;
                RaiseCanExecuteChanged();
            }
        }

        private string description;

        public string Description
        {
            get => description;

            set
            {
                description = value;
                RaiseCanExecuteChanged();
            }
        }

        private Task.StatusType status;

        /// <summary>
        /// The <see cref="Task.Status"/> of a <see cref="Task"/>
        /// </summary>
        public Task.StatusType Status
        {
            get => status;

            set
            {
                status = value;
                RaiseCanExecuteChanged();
            }
        }

        private double allocatedTime;

        /// <summary>
        /// The time (in minutes) allotted for this <see cref="Task"/>
        /// </summary>
        public double AllocatedTime
        {
            get => allocatedTime;

            set
            {
                allocatedTime = value;
                RaiseCanExecuteChanged();
            }
        }

        private User user;

        public User User
        {
            get => user;

            set
            {
                user = value;
                RaiseCanExecuteChanged();
            }
        }

        /// <summary>
        /// The possible values for a <see cref="Task"/> instance's <see cref="Task.Status"/> property 
        /// </summary>
        public static Task.StatusType[] TaskStatusTypes { get; }

        /// <summary>
        /// The possible values for a <see cref="Task"/> instance's <see cref="Task.User"/> property. Contains the 
        /// <see cref="User"/> instances associated with the currently selected <see cref="Project"/>
        /// </summary>
        public List<User> Users { get; set; }

        public DelegateCommand TaskPersistenceCommand { get; }

        #endregion

        static TaskInfoViewModel()
        {
            TaskStatusTypes = (Task.StatusType[]) Enum.GetValues(typeof(Task.StatusType));
        }

        public TaskInfoViewModel(ITaskService taskService, Project selectedProject, Action successfulTaskPersistenceCallback, Task selectedTask = null)
        {
            this.taskService = taskService;
            this.selectedProject = selectedProject;
            this.successfulTaskPersistenceCallback = successfulTaskPersistenceCallback;
            existingTask = selectedTask;

            Users = selectedProject.Team?.ToList() ?? new List<User>();

            TaskPersistenceCommand = new DelegateCommand(PersistTask, AreAllInputFieldsValid);

            if (selectedTask != null)
            {
                name = selectedTask.Name;
                description = selectedTask.Description;
                status = selectedTask.Status;
                allocatedTime = selectedTask.AllocatedTime;
                user = selectedTask.User;
            }
        }

        private bool AreAllInputFieldsValid()
        {
            return !StringUtil.AnyIsNullOrWhiteSpace(Name, Description) && AllocatedTime > 0 && User != null;
        }

        /// <summary>
        /// If <see cref="existingTask"/> is null, it creates and persists new <see cref="Task"/>, otherwise, the 
        /// existing <see cref="Task"/> instance (<see cref="existingTask"/>) is updated
        /// </summary>
        private void PersistTask()
        {
            Task task;

            if (existingTask != null)
            {
                existingTask.Name = Name;
                existingTask.Description = Description;
                existingTask.AllocatedTime = AllocatedTime;
                existingTask.User = User;
                existingTask.Status = Status;

                task = existingTask;
            }
            else
            {
                task = new Task(Name, Description, AllocatedTime, User, selectedProject, Status);
            }

            taskService.Save(task);
            successfulTaskPersistenceCallback();
        }

        private void RaiseCanExecuteChanged()
        {
            TaskPersistenceCommand.RaiseCanExecuteChanged();
        }
    }
}
