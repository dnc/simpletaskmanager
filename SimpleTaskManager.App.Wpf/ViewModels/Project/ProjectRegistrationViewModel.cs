﻿using System;
using SimpleTaskManager.App.Wpf.Commands;
using SimpleTaskManager.App.Wpf.Utils;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.ViewModels
{
    internal class ProjectRegistrationViewModel
    {
        private readonly IProjectService projectService;

        private readonly Action successfulProjectRegistrationCallback;

        private string projectName;

        public string ProjectName
        {
            get => projectName;

            set
            {
                projectName = value;
                ProjectRegistrationCommand?.RaiseCanExecuteChanged();
            }
        }

        public DelegateCommand ProjectRegistrationCommand { get; }

        public ProjectRegistrationViewModel(IProjectService projectService, Action successfulProjectRegistrationCallback)
        {
            this.projectService = projectService;
            this.successfulProjectRegistrationCallback = successfulProjectRegistrationCallback;

            ProjectRegistrationCommand = new DelegateCommand(RegisterProject, AreAllInputFieldsValid);
        }

        private bool AreAllInputFieldsValid()
        {
            return !StringUtil.AnyIsNullOrWhiteSpace(ProjectName);
        }

        private void RegisterProject()
        {
            projectService.Save(new Project(ProjectName));
            successfulProjectRegistrationCallback();
        }
    }
}
