﻿using System.Collections.ObjectModel;
using SimpleTaskManager.App.Wpf.Commands;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.ViewModels
{
    internal class ProjectListViewModel
    {
        private readonly IProjectService projectService;

        private readonly IUserProjectService userProjectService;

        private readonly User authenticatedUser;

        public ObservableCollection<ProjectInfo> Projects { get; }

        public DelegateCommand SelectedProjectRemovalCommand { get; }

        private ProjectInfo selectedProject;

        public ProjectInfo SelectedProject
        {
            get => selectedProject;

            set
            {
                selectedProject = value;
                SelectedProjectRemovalCommand?.RaiseCanExecuteChanged();
            }
        }

        public ProjectListViewModel(IProjectService projectService, IUserProjectService userProjectService, User authenticatedUser)
        {
            this.projectService = projectService;
            this.userProjectService = userProjectService;
            this.authenticatedUser = authenticatedUser;

            Projects = new ObservableCollection<ProjectInfo>();

            SelectedProjectRemovalCommand = new DelegateCommand(RemoveSelectedProject, IsSelectedProjectNotNull);

            ReloadProjects();
        }

        /// <summary>
        /// Updates the <see cref="Projects"/> list by re-reading the <see cref="Project"/> instance list via
        /// the provided service's <see cref="IProjectService.FindAll()"/> method
        /// </summary>
        public void ReloadProjects()
        {
            Projects.Clear();

            foreach (Project project in projectService.FindAll())
            {
                Projects.Add(new ProjectInfo(project, authenticatedUser, projectService, userProjectService));
            }
        }

        /// <summary>
        /// Checks if <see cref="SelectedProject"/> is not null
        /// </summary>
        /// <returns><see langword="true"/>, if <see cref="SelectedProject"/> is not null</returns>
        private bool IsSelectedProjectNotNull()
        {
            return SelectedProject != null;
        }

        /// <summary>
        /// Removes <see cref="SelectedProject"/> from the <see cref="Projects"/> list and via the provided service's
        /// <see cref="IProjectService.RemoveById(long)"/> method
        /// </summary>
        private void RemoveSelectedProject()
        {
            projectService.RemoveById(SelectedProject.Project.Id);
            Projects.Remove(SelectedProject);
        }

        /// <summary>
        /// A class that wraps useful <see cref="Project"/> class properties
        /// </summary>
        internal class ProjectInfo
        {
            public Project Project { get; }

            private readonly User authenticatedUser;

            private readonly IProjectService projectService;

            private readonly IUserProjectService userProjectService;

            private string name;

            public string Name
            {
                get => name;

                set
                {
                    if (name == value)
                    {
                        return;
                    }

                    name = value;
                    Project.Name = value;
                    projectService.Save(Project);
                }
            }

            public int TaskCount { get; set; }

            private bool isAssociatedWithAuthenticatedUser;

            public bool IsAssociatedWithAuthenticatedUser
            {
                get => isAssociatedWithAuthenticatedUser;

                set
                {
                    if (value == isAssociatedWithAuthenticatedUser)
                    {
                        return;
                    }

                    isAssociatedWithAuthenticatedUser = value;

                    if (value)
                    {
                        userProjectService.Associate(authenticatedUser, Project);
                    }
                    else
                    {
                        userProjectService.Disassociate(authenticatedUser, Project);
                    }
                }
            }

            public ProjectInfo(Project project, User authenticatedUser, IProjectService projectService,
                IUserProjectService userProjectService)
            {
                Project = project;

                this.authenticatedUser = authenticatedUser;
                this.projectService = projectService;
                this.userProjectService = userProjectService;

                Name = project.Name;
                TaskCount = project.Tasks?.Count ?? 0;

                IsAssociatedWithAuthenticatedUser = project.Team?.Contains(authenticatedUser) ?? false;
            }
        }
    }
}
