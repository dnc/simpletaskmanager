﻿using System;
using System.Windows;
using Microsoft.EntityFrameworkCore;
using SimpleTaskManager.App.Wpf.Commands;
using SimpleTaskManager.App.Wpf.Extensions;

using SimpleTaskManager.App.Wpf.Utils;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.ViewModels
{
    internal class UserRegistrationViewModel
    {
        private readonly IUserService userService;

        private readonly Action<string, string> successfulRegistrationCallback;

        private bool isRegistering;

        private string firstName;

        public string FirstName
        {
            get => firstName;

            set
            {
                firstName = value;
                RaiseCanExecuteChanged();
            }
        }

        private string lastName;

        public string LastName
        {
            get => lastName;

            set
            {
                lastName = value;
                RaiseCanExecuteChanged();
            }
        }

        private string email;

        public string Email
        {
            get => email;

            set
            {
                email = value;
                RaiseCanExecuteChanged();
            }
        }

        private string password;

        public string Password
        {
            get => password;

            set
            {
                password = value;
                RaiseCanExecuteChanged();
            }
        }

        public DelegateCommand RegistrationCommand { get; set; }

        public UserRegistrationViewModel(IUserService userService, Action<string, string> successfulRegistrationCallback)
        {
            this.userService = userService;
            this.successfulRegistrationCallback = successfulRegistrationCallback;

            RegistrationCommand = new DelegateCommand(RegisterUser, AreAllInputFieldsValid);
        }

        /// <summary>
        /// Checks if all of the properties that could be tied to the UI are correctly initialized
        /// </summary>
        /// <returns><see langword="true"/>, if all of the properties that could be tied to the UI are correctly 
        /// initialized</returns>
        private bool AreAllInputFieldsValid()
        {
            return !StringUtil.AnyIsNullOrWhiteSpace(FirstName, LastName, Email, Password)
                && Email.IsEmailAddress() && !isRegistering;
        }

        /// <summary>
        /// Saves a <see cref="User"/> by using the <see cref="IUserService.Save(User)"/> method of the provided
        /// <see cref="IUserService"/> implementation
        /// </summary>
        private void RegisterUser()
        {
            isRegistering = true;
            RaiseCanExecuteChanged();

            try
            {
                userService.Save(new User(FirstName, LastName, Email, Password));

                successfulRegistrationCallback(Email, Password);
            }
            catch (DbUpdateException e) when (e.InnerException.Message.Contains("duplicate key"))
            {
                /*
                 * When attempting to register an account with an email that already exists, the following is the
                 * message of the inner exception:
                 * 
                 * {"Cannot insert duplicate key row in object 'dbo.Users' with unique index 'IX_Users_Email'. 
                 * The duplicate key value is (john@example.com).\r\nThe statement has been terminated."}
                 * 
                 * We can thus use parts of this message to filter the exception that we catch
                 */

                MessageBox.Show("The specified email address is already in use", "Error",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }
            finally
            {
                isRegistering = false;
                RaiseCanExecuteChanged();
            }
        }

        private void RaiseCanExecuteChanged()
        {
            RegistrationCommand.RaiseCanExecuteChanged();
        }
    }
}
