﻿using System;
using System.Windows;
using SimpleTaskManager.App.Wpf.Commands;
using SimpleTaskManager.App.Wpf.Extensions;
using SimpleTaskManager.App.Wpf.Utils;
using SimpleTaskManager.Model.Models;
using SimpleTaskManager.Model.Services.Abstract;

namespace SimpleTaskManager.App.Wpf.ViewModels
{
    public class UserAuthViewModel
    {
        private readonly IUserService userService;

        private readonly Action<User> successfulLoginCallback;

        private bool isAuthenticating;

        private string _email;

        public string Email
        {
            get => _email;

            set
            {
                _email = value;
                RaiseCanExecuteChanged();
            }
        }

        private string _password;

        public string Password
        {
            get => _password;

            set
            {
                _password = value;
                RaiseCanExecuteChanged();
            }
        }

        public DelegateCommand AuthenticationCommand { get; }

        public UserAuthViewModel(IUserService userService, Action<User> successfulLoginCallback)
        {
            this.userService = userService;
            this.successfulLoginCallback = successfulLoginCallback;

            AuthenticationCommand = new DelegateCommand(Authenticate, AreAllInputFieldsValid);
        }

        private bool AreAllInputFieldsValid()
        {
            return !StringUtil.AnyIsNullOrWhiteSpace(Email, Password) && Email.IsEmailAddress()
                && !isAuthenticating;
        }

        /// <summary>
        /// Attempts to authenticate (<see cref="IUserService.CanAuthenticate(string, string)"/>) the user, based 
        /// on the provided credentials (<see cref="Email"/>, <see cref="Password"/>
        /// </summary>
        private void Authenticate()
        {
            isAuthenticating = true;
            RaiseCanExecuteChanged();

            if (userService.CanAuthenticate(Email, Password))
            {
                successfulLoginCallback(userService.FindByEmail(Email));
            }
            else
            {
                MessageBox.Show("The specified credentials are invalid", "Invalid credentials",
                    MessageBoxButton.OK, MessageBoxImage.Exclamation);
            }

            isAuthenticating = false;
            RaiseCanExecuteChanged();
        }

        private void RaiseCanExecuteChanged()
        {
            AuthenticationCommand.RaiseCanExecuteChanged();
        }
    }
}
