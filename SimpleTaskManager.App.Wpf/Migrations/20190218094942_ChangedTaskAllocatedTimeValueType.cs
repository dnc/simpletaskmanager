﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SimpleTaskManager.App.Wpf.Migrations
{
    public partial class ChangedTaskAllocatedTimeValueType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllottedTime",
                table: "Tasks");

            migrationBuilder.AddColumn<double>(
                name: "AllocatedTime",
                table: "Tasks",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "AllocatedTime",
                table: "Tasks");

            migrationBuilder.AddColumn<int>(
                name: "AllottedTime",
                table: "Tasks",
                nullable: false,
                defaultValue: 0);
        }
    }
}
